<?php
  session_start();
  unset($_SESSION['session_user']);
  unset($_SESSION['session_usr']);
  session_destroy();
  header("Location: studentlogin.php");
?>
