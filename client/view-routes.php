
<!DOCTYPE html>

<html>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="../css/spectre.min.css">
    <link rel="stylesheet" href="../css/spectre-exp.min.css">
    <link rel="stylesheet" href="../css/spectre-icons.min.css">
	 <link rel = "stylesheet"
         href = "https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <body>
    <?php
      include("headr.php");
    ?>
    <header>
      <h1> All Routes </h1>
    </header>
    <div class="form-group"  >
      <form action="" method="POST" >
    <select name="route" class="form-select" >
      Select your Route to see next bus
  <option value="Brampton-Lambton">Brampton-Lambton</option>
  <option value="Lambton-Brampton">Lambton-Brampton</option>
  <option value="Mississauga-Lambton">Mississauga-Lambton</option>
  <option value="Lambton-Mississauga">Lambton-Mississauga</option>
</select>
<input type="submit" class="btn btn-success" value="Next Bus"  class="form-group" >
</form>
</div>
    <?php

      $route = $_POST["route"];

      $dbhost = "localhost";
      $dbuser = "root";
      $dbpass = "root";
      $dbname = "buses";

      $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

      if (mysqli_connect_errno())
      {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
      }

      date_default_timezone_set('EST');

      $current_time = date("H:i:s");
$current_day = date("D");


      $query = "SELECT * FROM buses";
      //$query .= " ORDER BY hire_date ASC";
	    //$query1 = "SELECT pickup_time FROM buses WHERE pickup_time > $current_time ;";
//echo $query1;
      $results = mysqli_query($connection, $query);
		$results2 = mysqli_query($connection, $query);

      if ($results == FALSE) {
        echo "Database query failed. <br/>";
        echo "SQL command: " . $query;
        exit();
      }


	  $buses_time = mysqli_fetch_assoc($results);
    $pick_time = mysqli_fetch_assoc($results2);

    //print_r($pick_time);
	 // $userinfo = Array();

//while ($row = mysqli_fetch_array($results2, MYSQLI_ASSOC)) {
  //if($row['$current_day'])
   //test[] =  $row['pickup_time'];
//
$picktime= Array();
while ($row = mysqli_fetch_array($results2, MYSQLI_ASSOC)) {
    //$picklocation[] =  $row['pickup_location'];
    if($row['pickup_location'] == $route&&$row[$current_day] == "1"){
      $picktime[] = $row['pickup_time'];
    }
}


$storeArray = Array();
while ($row = mysqli_fetch_array($results2, MYSQLI_ASSOC)) {
    $storeArray[] =  $row['pickup_time'];
}
   //print_r($storeArray);
   //echo $current_time;

   $arr = Array();
   foreach ($picktime as $i)
   			{
   				if(substr($current_time,0,-6)<=substr($i, 0, -6))
   				{
            $arr[] = $i;
            sort($arr);
   					//echo "<br>".$i;




          /*  if(substr($current_time,-5,2)<=substr($i, -5, 2)&&(substr($current_time,0,-6)<=substr($i, 0, -6))){
            echo "<br>".substr($i, -5, 2);
           if(==substr($i, -5, 2))
     				{
              echo "ajj";
              echo substr($i, -2, 2);
            }*/
        //    echo "aaaa";

   				}

   			}
        foreach ($arr as $j) {
          if(substr($current_time,0,-6)==substr($j, 0, -6)){
            if(substr($current_time,-5,2)<substr($j, -5, 2))
            {
                $nextBus =  (substr($j, -5, 2))-(substr($current_time,-5,2));
            }
          }
        if (substr($current_time,0,-6)<substr($j, 0, -6)) {
        $nextBus =  ((substr($j, 0, -6)*60)+substr($j, -5, 2))-((substr($current_time,0,-6)*60)+(substr($current_time,-5,2)));
        break;
        }
        }
   //$storeArray
   /*if(substr($current_time,-5,2)>substr($arr[0], -5, 2)){
     echo "next bus at".$arr[1];
     echo "jgas";
   }
   else {
     echo "bus at".$arr[0];
   }
*/


    ?>
<marquee style="color:red" behavior="scroll" direction="left">
  <?php  if($nextBus>0){
  echo "Next bus will come in " .$nextBus ."minutes";
}
else {
  echo "Next Bus will come tommorow.";
} ?></marquee>

    <nav>

    </nav>

    <div class="container">
      <div class = "columns">
        <div class="column col-10 col-mx-auto">




          <table class="table ,table table-striped table-hover" style="background-color:lightblue" >
            <tr>

              <th>Pickup Time</th>
              <th>Bus Route</th>
              <th>Monday</th>
              <th>Tuesday</th>
              <th>Wednesday</th>
              <th>Thursday</th>
			  <th>Friday</th>
			  <th>Saturday</th>
			  <th>Sunday</th>
            </tr>

            <?php while ($bus = mysqli_fetch_assoc($results)) { ?>
              <tr>

                <td><?php echo $bus['pickup_time']; ?></td>
                <td><?php echo $bus['pickup_location']; ?></td>
                <?php if ($bus['Mon']==1) { ?>
				<td><i class="icon icon-check"></i></td>
				<?php } ?>
				<?php if ($bus['Mon']==0) { ?>
				<td><i class="icon icon-cross"></i></td>
				<?php } ?>

				<?php if ($bus['Tue']==1) { ?>
				<td><i class="icon icon-check"></i></td>
				<?php } ?>
				<?php if ($bus['Tue']==0) { ?>
				<td><i class="icon icon-cross"></i></td>
				<?php } ?>

				<?php if ($bus['Wed']==1) { ?>
				<td><i class="icon icon-check"></i></td>
				<?php } ?>
				<?php if ($bus['Wed']==0) { ?>
				<td><i class="icon icon-cross"></i></td>
				<?php } ?>

				<?php if ($bus['Thu']==1) { ?>
				<td><i class="icon icon-check"></i></td>
				<?php } ?>
				<?php if ($bus['Thu']==0) { ?>
				<td><i class="icon icon-cross"></i></td>
				<?php } ?>

				<?php if ($bus['Fri']==1) { ?>
				<td><i class="icon icon-check"></i></td>
				<?php } ?>
				<?php if ($bus['Fri']==0) { ?>
				<td><i class="icon icon-cross"></i></td>
				<?php } ?>

				<?php if ($bus['Sat']==1) { ?>
				<td><i class="icon icon-check"></i></td>
				<?php } ?>
				<?php if ($bus['Sat']==0) { ?>
				<td><i class="icon icon-cross"></i></td>
				<?php } ?>


               <?php if ($bus['Sun']==1) { ?>
				<td><i class="icon icon-check"></i></td>
				<?php } ?>
				<?php if ($bus['Sun']==0) { ?>
				<td><i class="icon icon-cross"></i></td>
				<?php } ?>

              </tr>


            <?php } ?>


			<?php while ($employee1 = mysqli_fetch_assoc($results)) {


				}
				?>








          </table>


        </div> <!--// col-12 -->
      </div> <!-- // column -->
    </div> <!--// container -->


    <?php
      // clean up and close database
      mysqli_free_result($results);
      mysqli_close($connection);
    ?>

  </body>


</html>
<?php
  include("footer.php");
?>
