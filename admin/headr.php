<?php
session_start();
if(!isset($_SESSION["session_user"]))
{
  header("Location: adminlogin.php");
}
else
{
?>
<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width">


      <link rel="stylesheet" href="../css/spectre.min.css">
      <link rel="stylesheet" href="../css/spectre-exp.min.css">
      <link rel="stylesheet" href="../css/spectre-icons.min.css">

      <title></title>

      <style type="text/css">
        *
        {
          margin: 0;
          padding: 0;
        }

        body
        {
          font-family: sans-serif;
        }

        .nav-bar
        {
          background: rgb(0, 0, 0, 0.8);
          height: 100px;
          background-color: #104CB2;
        }

        .logo
        {
          margin: 10px 50px;
          height: 60px;
        }
        .menu
        {
          float: right;
          list-style: none;
          margin: 15px;
          margin-right: 20px;
          background-color: #104CB2;
        }

        .menu li
        {
           display: inline-block;
           margin: 10px 5px;
        }

        .menu li a
        {
           text-decoration: none;
           color: white;
           padding: 5px 10px;
           letter-spacing: 2px;
           border: 1px solid #fff;
        }

        .menu li a:hover
        {
           background: #fff;
           transition: .4s;
           color: #000;
        }


        .menu li button
        {
           text-decoration: none;
           color: white;
           padding: 2px 5px;
           letter-spacing: 2px;
           border: 1px solid #fff;
           background-color: #104CB2;
        }

        .menu li button:hover
        {
           background: #fff;
           transition: .4s;
           color: #000;
        }

        .welcome
        {
          position: relative;
          text-align: center;
          color: #fff;
          top: 20px;
        }

        .welcome h1
        {
          font-size: 52px;
          margin: 25px;
          font-weight: bold;
          text-shadow: -2px 2px 2px #D9D4CF;
        }

        footer
        {
           font-size:14px;
           height: 75px;
           margin-bottom: 0px;
           text-align: center;
           padding-top: 10px ;
           padding-bottom: 5px;
           color: white;
           background-color:#767676;
           position: fixed;
           right: 0;
           bottom: 0;
           width:100%;
           left: 0;
           padding: 1rem;
        }



        .top
        {
           position: relative;
           text-align: center;
           color: #767676;
           top: 20px;
        }

        .top h1
        {
           font-size: 52px;
           margin: 25px;
           padding-bottom: 10px;
           font-weight: bold;
           opacity: 0.5;
           line-height: 120%;
           text-decoration: underline;
           text-shadow: -3px -5px 2px #D9D4CF;
        }


        .simple-form
        {
            text-align: center;
            margin: auto;
            width: 50%;
            padding: 10px;

        }
        #registration
        {
          padding: 50px 0px;
          background: rgba(0,0,0,.8);
          box-sizing: border-box;
          box-shadow: 0 15px 25px rgba(0,0,0,.5);
          border-radius: 10px;
          margin-bottom: -30px;
        }

        #button
        {
          width: 250px;
          padding: 10px;
          border-radius: 5px;
        }
        .simple-form input[type="submit"]
        {
            border: none;
            outline: none;
            color: #fff;
            background: #03a9f4;
            height: 50px;
            font-size: 18px;
            cursor: pointer;
            border-radius: 30px;
            margin-top: 10px;
            width: 250px;
        }

        .simple-form input[type="submit"]:hover
        {
          cursor: pointer;
          background: #ff8a00;
          color: #fff;
        }

        .wrapper
        {
           margin:3em 0;

        }
        .wrapper1
        {
           margin:-1em 0;
        }
        .wrapper2
        {
           margin:-2em 0;
        }


        #a,#a:visited,#a:hover,#a:active
        {
            -webkit-backface-visibility:hidden;
            backface-visibility:hidden;
          	position:relative;
            transition:0.5s color ease;
          	text-decoration:none;
          	color:black;
          	font-size:1.25em;
            font-weight:bold;
            margin-right:30px;
            margin-bottom: 30px;
            float:right;
        }

        #a:hover
        {
  	        color:#133EB9;
            border:none;
        }
        #a.after:hover:after
        {
           width:100%;
           border:none;
        }
        #a:after
        {
           width:100%;
           right:0;
           transition:0.25s all ease 0.4s;
           border:none;
        }

        #a.after:after
        {
           bottom:0.25em;
           height:5px;
           height:0.20rem;
           width:0;
           background:#133EB9;
           content: "";
           transition:0.5s all ease;
           -webkit-backface-visibility:hidden;
            backface-visibility:hidden;
            position:absolute;
            border:none;
        }
        #a.first:after
        {
          left:0;
        }

        #a.second:after
        {
          right:0;
        }


      </style>
    </head>
    <body>
      <header>
        <div class="nav-bar">
          <img src="http://www.edwiseinternational.com/study-abroad-resources/lambton_college_logo.jpg" class="logo">
          <ul class= "menu">
            <li><a href="adminhome.php">Home</a></li>
            <li><a href="view-routes.php">Bus Info</a></li>
            <li><a href="send-sms.php">Send SMS</a></li>
            <li><a href="adminstudent.php">Student Info</a></li>
            <li><a href="adminlogout.php">Log Out</a></li>
          </ul>
        </div>
        <?php
        }
        ?>
        <script>
function myFunction() {

	if(confirm("Press a button!"))
	{
		window.location.href = "sms.php";
	}
else
{
	console.log("yes");
}
}
</script>
