<?php
  include("headr.php");
?>
<div class="top">
  <h1>INFO OF A STUDENT</h1>
</div>
</header>
<style>

  {}
  .top
  {
     position: relative;
     text-align: center;
     color: #767676;
     top: 20px;
  }

  .top h1
  {
     font-size: 52px;
     margin: 25px;
     padding-bottom: 10px;
     font-weight: bold;
     opacity: 0.5;
     line-height: 120%;
     text-decoration: underline;
     text-shadow: -3px -5px 2px #D9D4CF;
  }

  .show-info
  {
    margin: auto;
    padding-top: 60px;
    padding-bottom: 20px;
    padding-left: 200px;
    padding-right: 20px;
    margin-top: 40px;

  }

  .show-info p
  {
      color: #fff;
      font-family: Baskerville, "Baskerville Old Face", "Hoefler Text", Garamond, "Times New Roman", serif;
	    font-size: 24px;
	    font-style: normal;
	    font-variant: normal;
	    line-height: 24.2px;
  }

  .show-info p strong
  {
      color: #fff;
      font-family: Rockwell, "Courier Bold", Courier, Georgia, Times, "Times New Roman", serif;
	    font-size: 28px;
	    font-style: normal;
	    font-variant: small-caps;
	    font-weight: 550;
	    line-height: 30px;
  }

  .back
  {
    border-radius: 50px;
    background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 100, 0.5)), url("https://i.stack.imgur.com/p9mUO.jpg");
    background-repeat: no-repeat;
    height: 45vh;
    background-size: cover;
    margin-top: 100px;
    margin-bottom: 150px;
    background-position: center;
  }

  </style>


    <div class="container ">
      <div class="column">
        <div class="column col-10 col-mx-auto">
          <div class="wrapper1" style="margin-top:-70px;float:left;">
              <a id="a" href="adminstudent.php" class="second after"> <<< Go Back  </a>
            </div>
            <div class="back">
          <?php
              require("db_credentials.php");
              $connection = connect();
              $results =showStudents();

              while ($student = mysqli_fetch_assoc($results))
              {
                //print_r($employee["first_name"]);
                echo "<div class='show-info'>";
                echo "<p><strong>Student Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>" . $student["student_name"] . "</p>";
                echo "<p><strong>Mobile No.: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>" . $student["mobile_no"] . "</p>";
                echo "<p><strong>Username or Email-id: &nbsp;&nbsp;</strong>" . $student["username"] . "</p>";
                echo "<p><strong>Password: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>" . $student["password"] . "</p>";
                echo "</div>";
              }
          ?>
        </div>
        </div> <!--//col-10-->
      </div> <!--//columns -->
    </div> <!--// container -->


    <?php
      include("footer.php");
    ?>
