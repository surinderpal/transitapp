<?php
if(isset($_POST["submit"]))
{
  $user=$_POST['user'];
  $pass=$_POST['pass'];

  require("db_credentials.php");

  $connection = connect();
  $sql = "SELECT * FROM admin WHERE username = '$user' AND password = '$pass'";
  $result = mysqli_query($connection, $sql);
  $numrows=mysqli_num_rows($result);

  if($numrows != 0)
  {
    while($row = mysqli_fetch_assoc($result))
    {
      $dbusername = $row['username'];
      $dbpassword = $row['password'];
    }
    if($user = $dbusername && $pass = $dbpassword)
    {
      session_start();
      $_SESSION['session_user'] = $user;
      header("Location: adminhome.php");
    }
  }
  else
  {
    echo "Invalid username or password!";

  }
  }


?>


<!doctype html>
<html>
  <head>
      <meta charset="utf-8">
      <title>Admin Login</title>
      <style type="text/css">
      body
      {
        margin: 0;
        padding: 0;
        font-family: sans-serif;
        background: url("http://www.kothanalloorforanechurch.com/assets/images/secure.jpg");
        background-repeat: no-repeat;
        background-size: 100% 850px;
      }

      .box
      {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        width: 400px;
        padding: 40px;
        background: rgba(0,0,0,.8);
        box-sizing: border-box;
        box-shadow: 0 15px 25px rgba(0,0,0,.5);
        border-radius: 10px;
      }

      .box h2
      {
        margin: 0 0 30px;
        padding: 0;
        color: #fff;
        text-align: center;
      }

      .box .inputBox
      {
        position: relative;
      }

      .box .inputBox input
      {
        width: 100%;
        padding:10px 0;
        font-size: 16px;
        color: #fff;
        left-spacing: 1px;
        margin-bottom: 30px;
        border: none;
        border-bottom: 1px solid #fff;
        outline: none;
        background: transparent;
      }

      .box .inputBox label
      {
          position: absolute;
          top: 0;
          left: 0;
          padding: 10px 0;
          font-size: 16px;
          color: #fff;
          pointer-events: none;
          transition: .5s;
      }

      .box .inputBox input:focus ~ label,
      .box .inputBox input:valid ~ label
      {
           top: -18px;
           left: 0;
           color: #03a9f4;
           font-size: 12px;
      }

      .box input[type="submit"]
      {
          background: transparent;
          border: none;
          outline: none;
          color: #fff;
          background: #03a9f4;
          padding: 10px 20px;
          cursor: pointer;
          border-radius: 5px;
      }

      .box input[type="submit"]:hover
      {
        cursor: pointer;
        background: #ff8a00;
        color: #fff;
      }

      </style>
  </head>
  <body>
    <div class="box">
      <h2>Admin Login</h2>
      <form action=""<?php echo "adminlogin.php"?> method="POST">
        <div class="inputBox">
        <input type="email" pattern="[^ @]*@[^ @]*" name="user" required="">
        <label>Username</label>
        </div>
        <div class="inputBox">
        <input type="password" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*\W).{8,}$" name="pass" required="">
        <label>Password</label>
        </div>
        <input type="submit" name="submit" value="Login">
      </form>
    </div>
  </body>
</html>
