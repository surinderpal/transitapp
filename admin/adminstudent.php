<?php
  include("headr.php");
?>
<div class="top">
  <h1>STUDENT INFORMATION</h1>
</div>
</header>

    <?php
      require("db_credentials.php");
    ?>
    <?php
         $connection = connect();
         $results = getStudentsInformation();
    ?>
    <style>


    .btn1
    {
       text-decoration: none;
       color: #133EB9;
       padding: 5px 10px;
       letter-spacing: 2px;
       border: 1px solid #133EB9;
       padding: 10px;
    }

    .btn1:hover
    {
      text-decoration: none;
      border: 1px solid #FF8500;
      background: #FF8500;
      transition: .2s;
      color: #fff;

    }
    td,th
    {
      text-align: center;

    }
    </style>

    <div class="container">
      <div class = "columns">
        <div class="column col-10 col-mx-auto">
          <div class="wrapper">
              <a id="a" href="addNewStudent.php" class="first after">ADD STUDENT >>>  </a>
            </div>
          <table align="center" style="margin-bottom:200px;" class="table table-striped ">
            <col width="40">
            <col width="150">
            <col width="200">
            <col width="200">
            <col width="100">
            <col width="150">
            <col width="150">
            <col width="150">
            <tr>
              <th height="60">ID</th>
              <th height="60">Name</th>
              <th height="60">Mobile No.</th>
              <th height="60">Username/Email</th>
              <th height="60">Password</th>
              <th height="60">&nbsp;</th>
              <th height="60">&nbsp;</th>
              <th height="60">&nbsp;</th>
            </tr>

            <!-- we got a results from the database, so put them into the table -->
            <?php while ($student = mysqli_fetch_assoc($results)) { ?>
              <tr>
                <td height="60"><?php echo $student['id']; ?></td>
                <td height="60"><?php echo $student['student_name']; ?></td>
                <td height="60"><?php echo $student['mobile_no']; ?></td>
                <td height="60"><?php echo $student['username']; ?></td>
                <td height="60"><?php echo $student['password']; ?></td>
                <td height="60"><a class="action btn1" href="<?php echo 'show.php?id=' . $student['id']; ?>">View</a></td>
                <td height="60"><a class="action btn1" href="<?php echo 'edit.php?id=' . $student['id']; ?>">Edit</a></td>
                <td height="60"><a class="action btn1" href="<?php echo 'delete.php?id=' . $student['id']; ?>">Delete</a></td>
              </tr>
            <?php } ?>

          </table>

        </div> <!--// col-12 -->
      </div> <!-- // column -->

    </div> <!--// container -->


    <?php
      // clean up and close database
      mysqli_free_result($results);
      mysqli_close($connection);
    ?>
    <?php
      include("footer.php");
    ?>
