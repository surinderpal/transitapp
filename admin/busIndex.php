
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="spectre.min.css">
    <link rel="stylesheet" href="spectre-exp.min.css">
    <link rel="stylesheet" href="spectre-icons.min.css">
  </head>
  <body>
    <header>
      <h1> ADMIN AREA </h1>
    </header>

    <nav>
      <ul>
        <li>
          <a href="allRoutes.php">Show All Routes</a>
        </li>
        <li>
          <a href="modifyRoute.php">Modify A Route</a>
        </li>
		 <li>
          <a href="addRoute.php">Add a Route</a>
        </li>
		 <li>
          <a href="deleteRoute.php">Delete a Route</a>
        </li>
		<?php
   $date = date("Y-m-d H:i:s");
?>
		
      </ul>
    </nav>

    <div class="container">
      <div class = "columns">
        <div class="column col-10 col-mx-auto">

          <?php

          ?>

        </div> <!--// col-12 -->
      </div> <!-- // column -->
    </div> <!--// container -->

    <footer>
      &copy; <?php echo date("Y") ?> Cestar College
    </footer>

  </body>
</html>
