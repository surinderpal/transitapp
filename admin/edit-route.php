
<?php
//if (isset($_GET["id"]) == FALSE) {
  // missing an id parameters, so
  // redirect person back to add employee page
//  header("Location: " . "allRoutes.php");
//  exit();
//}
$id = $_GET["id"];

  // do the SQL
  $dbhost = "localhost";
  $dbuser = "root";
  $dbpass = "root";
  $dbname = "buses";

  $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

  if (mysqli_connect_errno())
  {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
  }
  $sql =  "SELECT * from buses";
  $sql .= " WHERE id='" . $id . "'";

  $results = mysqli_query($connection, $sql);

  if ($results == FALSE) {
    echo "Database query failed. <br/>";
    echo "SQL command: " . $sql;
    exit();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // get items from DATABASE
    $busRoute = [];
    $busRoute["bus_time"] = $_POST['picktime'];
    $busRoute["route"] = $_POST['location'];


    $monday = 0;
    $tuesday = 0;
    $wednesday = 0;
    $thursday = 0;
    $friday = 0;
    $saturday = 0;
    $sunday = 0;

    foreach($_POST['check_list'] as $i){
  if($i == "Mon"){
    $monday = 1;
  }
  if($i == "Tue"){
    $tuesday = 1;
  }
  if($i == "Wed"){
    $wednesday = 1;
  }
  if($i == "Thu"){
    $thursday = 1;
  }
  if($i == "Fri"){
    $friday = 1;
  }
  if($i == "Sat"){
    $saturday = 1;
  }
  if($i == "Sun"){
    $sunday = 1;
  }
  }
  $busRoute["Mon"] = $monday;
  $busRoute["Tue"] = $tuesday;
  $busRoute["Wed"] = $wednesday;
  $busRoute["Thu"] = $thursday;
  $busRoute["Fri"] = $friday;
  $busRoute["Sat"] = $saturday;
  $busRoute["Sun"] = $sunday;

    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "root";
    $dbname = "buses";

    $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
      exit();
    }

    $query = "UPDATE buses SET  ";
    $query .= "pickup_time='" . $busRoute["bus_time"] . "', ";
    $query .= "pickup_location='" . $busRoute["route"] . "', ";
    $query .= "Mon='" . $busRoute["Mon"] . "', ";
    $query .= "Tue='" . $busRoute["Tue"] . "', ";
    $query .= "Wed='" . $busRoute["Wed"] . "', ";
    $query .= "Thu='" . $busRoute["Thu"] . "', ";
    $query .= "Fri='" . $busRoute["Fri"] . "', ";
    $query .= "Sat='" . $busRoute["Sat"] . "', ";
    $query .= "Sun='" . $busRoute["Sun"] . "' ";
    $query .= " WHERE id='" . $id . "'";
    $query .= " LIMIT 1";

    $results = mysqli_query($connection, $query);

    // for delete statements, the result is going to be true or false.
  if ($results == TRUE) {
     header("Location: view-routes.php");
     exit();
    }
    if ($results == FALSE) {
      echo "Database query failed. <br/>";
      echo "SQL command: " . $query;
      exit();
    }

    mysqli_free_result($results);
    mysqli_close($connection);
  }

?>
<?php
  include("headr.php");
?>


<div class="top">
  <h1>UPDATE BUS INFO</h1>
</div>
</header>
<style>

  .top
  {
     position: relative;
     text-align: center;
     color: #767676;
     top: 20px;
  }

  .top h1
  {
     font-size: 52px;
     margin: 25px;
     padding-bottom: 10px;
     font-weight: bold;
     opacity: 0.5;
     line-height: 120%;
     text-decoration: underline;
     text-shadow: -3px -5px 2px #D9D4CF;
  }

    table
    {
      margin: auto;
    }
    td
    {
      padding : 5px;
      text-align: center;
    }

    tr
    {
      padding: 5px;

    }

</style>



<div class="container">
  <div class="columns">
    <div class="column col-10 col-mx-auto">
      <div class="wrapper1" style="margin-top:20px;float:left;">
          <a id="a" href="view-routes.php" class="second after"> <<< Go Back  </a>
      </div>

      <div style="margin-bottom:200px;margin-top:70px;" class="simple-form">
        <form action="" id="registration" method="POST" class="form-group">
        <table cellpadding= 10px>
          <tr>
            <td>
              <label style="color:white;" class="form-label" for="picktime">PICKUP TIME</label>
            </td>
            <td>
              <input type="time" name="picktime" style="padding-left:15px;border-radius:5px;" value="<?php echo $busRoute['pickup_time']?>" required/>
            </td>
          </tr>
          <tr>
            <td>
              <label style="color:white" class="form-label" for="picktime">Pick up Location and Route</label>
            </td>
            <td>
              <select name="location" value="<?php echo $busRoute['pickup_location']?>" style="border-radius:5px;" class="form-select" required>
                  <option value="Bramp-Lamb">Brampton-Lambton</option>
                  <option value="Lamb-Bram">Lambton-Brampton</option>
                  <option value="Lamb-Missi">Lambton-Mississauga</option>
                  <option value="Missi-Lamb">Mississauga-Lambton</option>
              </select>
            </td>
          </tr>
          <tr>

            <td colspan=2>
              <label style="color:white;font-family: Helvetica georgia;font-weight:bold; font-size: 20px;" class="form-label" for="daysava"> DAYS AVAILABLE</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="Mon">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Monday</label><br/>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="Tue">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Tuesday</label><br/>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="Wed">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Wednesday</label><br/>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="Thu">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Thursday</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="Fri">
            </td>
            <td>
                <label class="form-checkbox" style="color:white;">Friday</label>
            </td>
          </tr>
          <tr>
            <td>
                <input type="checkbox" name="check_list[]" value="Sat">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Saturday</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="Sun">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Sunday</label>
            </td>
          </tr>
          <tr>
            <td colspan=2>
              <input type="submit" value="Update Bus Route" class="btn btn-success"/>
            </td>
          </tr>
        </table>
      </form>
    </div>

    </div> <!--//col-10-->
  </div> <!--//columns -->
</div> <!--// container -->

    <?php
      include("footer.php");
    ?>
