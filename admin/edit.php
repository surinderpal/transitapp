<?php
require("db_credentials.php");
?>
<?php
if (isset($_GET["id"]) == FALSE)
{
  header("Location: " . "adminstudent.php");
  exit();
}


$id = $_GET["id"];

$connection = connect();

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    $results = showStudents();
    print_r($results);
    $person = mysqli_fetch_assoc($results);
}
elseif ($_SERVER["REQUEST_METHOD"] == "POST")
{
  updateStudentInformation();
}
?>

<?php
  include("headr.php");
?>
<div class="top">
  <h1>UPDATE STUDENT INFO</h1>
</div>
</header>
<style>

  .top
  {
     position: relative;
     text-align: center;
     color: #767676;
     top: 20px;
  }

  .top h1
  {
     font-size: 52px;
     margin: 25px;
     padding-bottom: 10px;
     font-weight: bold;
     opacity: 0.5;
     line-height: 120%;
     text-decoration: underline;
     text-shadow: -3px -5px 2px #D9D4CF;
  }
  </style>
    <div class="container">
      <div class = "columns">
        <div class="column col-10 col-mx-auto">

          <div class="wrapper1" style="margin-top:20px;float:left;">
              <a id="a" href="adminstudent.php" class="second after"> <<< Go Back  </a>
            </div>

          <div style="margin-bottom:200px;margin-top:70px;" class="simple-form">
          <!-- @TODO: fill in the ACTION and METHOD portions -->
          <form action="<?php echo "edit.php?id=" . $id; ?>" method="POST" id="registration">

                  <!-- @TODO: fill in the value="" parameters -->

                  <input type="text" name="Name" id="button" value="<?php echo $person['student_name']; ?>" required/><br><br>
                  <input type="tel" name="MobileNo" id="button" value="<?php echo $person['mobile_no']; ?>" required /><br><br>
                  <input type="email" id="button" pattern="[^ @]*@[^ @]*"  name="UserName" value="<?php echo $person['username']; ?>" required/><br><br>
                  <input type="text" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*\W).{8,}$" id="button" name="PassWord" value="<?php echo $person['password']; ?>" required/><br><br>
                  <input type="submit" value="Update Student" />
            </p>
          </form>
        </div>

        </div> <!--// col-12 -->
      </div> <!-- // column -->
    </div> <!--// container -->
    <?php
      include("footer.php");
    ?>

    <?php
    /*
      $id = $_GET["id"];
      if ($_SERVER['REQUEST_METHOD'] == 'POST')
      {
        updateStudentInformation();
      }
      */
    ?>
