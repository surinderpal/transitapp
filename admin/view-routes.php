<?php
   include("headr.php");
?>
<div class="top">
  <h1>BUS ROUTES</h1>
</div>
    </header>
    <?php
      require("db_credentials.php");
    ?>
    <?php
         $connection = connect();
         $results = getBusesInformation();
    ?>

    <style>


    .btn1
    {
       text-decoration: none;
       color: #133EB9;
       padding: 5px 10px;
       letter-spacing: 2px;
       border: 1px solid #133EB9;
       padding: 10px;
    }

    .btn1:hover
    {
      text-decoration: none;
      border: 1px solid #FF8500;
      background: #FF8500;
      transition: .2s;
      color: #fff;

    }
    td,th
    {
      text-align: center;

    }
    </style>


    <div class="container">
      <div class = "columns">

        <div class="column col-10 col-mx-auto">
          <div class="wrapper">
            <a id="a" href="add-route.php"  class="first after"> ADD BUS ROUTE >>> </a>
          </div>

          <table align="center" style="margin-bottom:200px;" class="table table-striped ">
            <col width="100">
            <col width="200">
            <col width="40">
            <col width="40">
            <col width="40">
            <col width="40">
            <col width="40">
            <col width="40">
            <col width="40">
            <col width="150">
            <col width="150">
            <tr>
              <th height="60">Pickup Time</th>
              <th height="60">Bus Route</th>
              <th height="60">Monday</th>
              <th height="60">Tuesday</th>
              <th height="60">Wednesday</th>
              <th height="60">Thursday</th>
			        <th height="60">Friday</th>
      			  <th height="60">Saturday</th>
      			  <th height="60">Sunday</th>
              <th height="60"></th>
              <th height="60"></th>
            </tr>

            <?php while ($bus = mysqli_fetch_assoc($results)) { ?>
              <tr>
                <td height="60"><?php echo $bus['pickup_time']; ?></td>
                <td height="60"><?php echo $bus['pickup_location']; ?></td>

                <?php if ($bus['Mon'] == 1) { ?>
				           <td height="60"><i class="icon icon-check"></i></td>
                <?php } else { ?>
				           <td height="60"><i class="icon icon-cross"></i></td>
				        <?php } ?>

				        <?php if ($bus['Tue'] == 1) { ?>
				           <td height="60"><i class="icon icon-check"></i></td>
				        <?php } else { ?>
			             <td height="60"><i class="icon icon-cross"></i></td>
				        <?php } ?>

                <?php if ($bus['Wed'] == 1) { ?>
				           <td height="60"><i class="icon icon-check"></i></td>
				        <?php } else { ?>
			             <td height="60"><i class="icon icon-cross"></i></td>
				        <?php } ?>

                <?php if ($bus['Thu'] == 1) { ?>
				           <td height="60"><i class="icon icon-check"></i></td>
				        <?php } else { ?>
			             <td height="60"><i class="icon icon-cross"></i></td>
				        <?php } ?>

                <?php if ($bus['Fri'] == 1) { ?>
				           <td height="60"><i class="icon icon-check"></i></td>
				        <?php } else { ?>
			             <td height="60"><i class="icon icon-cross"></i></td>
				        <?php } ?>

                <?php if ($bus['Sat'] == 1) { ?>
				           <td height="60"><i class="icon icon-check"></i></td>
				        <?php } else { ?>
			             <td height="60"><i class="icon icon-cross"></i></td>
				        <?php } ?>

                <?php if ($bus['Sun'] == 1) { ?>
				           <td height="60"><i class="icon icon-check"></i></td>
				        <?php } else { ?>
			             <td height="60"><i class="icon icon-cross"></i></td>
				        <?php } ?>

                <td height="60"><a class="action btn1" href="<?php echo 'edit-route.php?id=' . $bus['id']; ?>" >Edit</a></td>
                <td height="60"><a class="action btn1" href="<?php echo 'delete-route.php?id=' . $bus['id']; ?>">Delete</a></td>
            </tr>
            <?php } ?>
      </table>

        </div> <!--// col-12 -->
      </div> <!-- // column -->
    </div> <!--// container -->
    <?php
      // clean up and close database
      mysqli_free_result($results);
      mysqli_close($connection);
    ?>
    <?php
      include("footer.php");
    ?>
